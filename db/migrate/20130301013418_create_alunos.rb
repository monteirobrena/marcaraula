class CreateAlunos < ActiveRecord::Migration
  def change
    create_table :alunos do |t|
      t.string :cpf
      t.string :nome
      t.string :matricula
      t.date :dataNascimento
      t.date :dataInicioPauta

      t.timestamps
    end
  end
end
