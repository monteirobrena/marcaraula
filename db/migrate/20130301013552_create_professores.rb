class CreateProfessores < ActiveRecord::Migration
  def change
    create_table :professores do |t|
      t.string :cpf
      t.string :nome
      t.string :matricula

      t.timestamps
    end
  end
end
