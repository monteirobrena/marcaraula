class CreateEscolas < ActiveRecord::Migration
  def change
    create_table :escolas do |t|
      t.string :cnpj
      t.string :nomeFantasia
      t.string :razaoSocial
      t.string :nomeResponsavel
      t.string :telefone
      t.string :endereco
      t.string :email
      t.string :site

      t.timestamps
    end
  end
end
