class CreateAulas < ActiveRecord::Migration
  def change
    create_table :aulas do |t|
      t.string :tipo
      t.datetime :dataHorario

      t.timestamps
    end
  end
end
