class Escola < ActiveRecord::Base
  attr_accessible :cnpj, :email, :endereco, :nomeFantasia, :nomeResponsavel, :razaoSocial, :site, :telefone
end
